package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        List<Thread> threads = new ArrayList<>();
        int threadNum = 7;
        final List<Long> countNumbers = new ArrayList<>();
        long singleThreadSample = numSamples/threadNum;
        ThreadLocalRandom tlr = ThreadLocalRandom.current();

        for (int i = 0; i < threadNum; i++){
            Runnable runnable = new Runnable() {
                long numInsideCircle = 0;

                @Override
                public void run() {
                    for (long i = 0; i < singleThreadSample; i++) {

                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();

                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }
                    }
                    countNumbers.add(numInsideCircle);
                }
            };
            Thread thread = new Thread(runnable);
            threads.add(thread);
        }


        for (Thread thread: threads){
            thread.start();
        }



        for (Thread thread: threads){
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        long totalNumberInsideCircle = 0;
        for (long a: countNumbers){
            totalNumberInsideCircle += a;
        }

        double estimatedPi = 4.0 * (double) totalNumberInsideCircle / (double) numSamples;

        return estimatedPi;
        // TODO Implement this.
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
