package ictgradschool.industry.lab13.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by User on 7/05/2017.
 */
public class ConcurrentBankingApp {
    public static void main(String[] args) {


        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        final BankAccount account = new BankAccount();
        final List<Double> changes = new ArrayList<>();


        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {

                List<Transaction> transactions = TransactionGenerator.readDataFile();
                for (Transaction trans : transactions) {
                    try {
                        queue.put(trans);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread consumer1 = new Thread(new Consumer(queue, account));
        Thread consumer2 = new Thread(new Consumer(queue, account));
        producer.start();
        consumer1.start();
        try {
            consumer1.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        consumer2.start();
        try {
            consumer2.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        try {
            producer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        consumer1.interrupt();
        consumer2.interrupt();
        try {
            consumer1.join();
            consumer2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Final balance: " + account.getFormattedBalance());




    }
}



