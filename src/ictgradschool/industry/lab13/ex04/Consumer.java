package ictgradschool.industry.lab13.ex04;

import ictgradschool.industry.lab13.examples.example03.IComputeTask;

import java.util.concurrent.BlockingQueue;

/**
 * Created by User on 7/05/2017.
 */
public class Consumer implements Runnable{


    private BlockingQueue<Transaction> queue;
    private BankAccount bankAccount;

    public Consumer(BlockingQueue<Transaction> queue, BankAccount bankAccount) {
        this.queue = queue; this.bankAccount = bankAccount;
    }

    @Override
    public void run() {

            // Continually...

            while (true) {

                // Grab the next task to execute on this thread
                Transaction action = queue.poll();

                if (action != null) {
                    switch (action._type) {
                        case Deposit:
                            bankAccount.deposit(action._amountInCents);
                            break;
                        case Withdraw:
                            bankAccount.withdraw(action._amountInCents);
                            break;
                    }
                }
                else if (Thread.currentThread().isInterrupted()){
                    break;
                }
            }
        }
    }


