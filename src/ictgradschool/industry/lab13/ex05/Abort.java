package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab13.examples.example03.PrimeFactors;

/**
 * Created by mche618 on 8/05/2017.
 */
public class Abort implements Runnable {
    private Thread targetThread;


    public Abort(Thread target){
        this.targetThread= target;
    }


    @Override
    public void run(){
        System.out.println("If you can not be bother to wait for Calculation, please type \"disconnect\"");
        while (!Thread.currentThread().isInterrupted()){
        String userRequest ="";
        userRequest = (Keyboard.readInput()).toLowerCase();
        if ( userRequest.equals("disconnect")){
            targetThread.interrupt();
            break;
        }
    }
}
}
