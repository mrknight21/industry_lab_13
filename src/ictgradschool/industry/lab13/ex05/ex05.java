package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.util.Set;

/**
 * Created by mche618 on 8/05/2017.
 */
public class ex05 {
    public static void main(String[] args) {

        long n =0;
        System.out.println("Please enter a number that you are interested in finding out its prime factors: ");
        while(true) {
            try {
                n = Long.parseLong(Keyboard.readInput());
                System.out.println("You just entered "+n+". Please wait......");
                break;
            } catch (NumberFormatException e) {
                System.out.println("The system only accept numbers without any other symbols or characters");
            }
        }



        PrimeFactorsTask cal = new PrimeFactorsTask(n);
        Thread thread1 = new Thread(cal);
        Abort request = new Abort(thread1);
        Thread thread2 = new Thread(request);
        thread2.start();
        thread1.start();



        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread2.interrupt();
        try {
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("The program ends");

    }
}
