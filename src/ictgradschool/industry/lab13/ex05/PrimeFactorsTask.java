package ictgradschool.industry.lab13.ex05;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by mche618 on 8/05/2017.
 */
public class PrimeFactorsTask implements Runnable {

    public enum TaskState {Initialised, Completed, Aborted}

    private Set<Long> primeFactors;
    private TaskState taskState;
    private Long n;


    public PrimeFactorsTask(long n) {
        this.n = n;
        this.primeFactors = new TreeSet<>();
        this.taskState = TaskState.Initialised;
    }

    @Override
    public void run() {
        // for each potential factor
        for (long factor = 2; factor * factor <= this.n && (!Thread.currentThread().isInterrupted()); factor++) {

            // if factor is a factor of n, repeatedly divide it out
            while (n % factor == 0 && (!Thread.currentThread().isInterrupted())) {
                primeFactors.add(factor);
                n = n / factor;
            }

        }

        if (!Thread.currentThread().isInterrupted()) {
            if (n > 1) primeFactors.add(n);
            this.taskState = TaskState.Completed;
            System.out.println("The prime factors are: ");
            for (long a : primeFactors){
                System.out.print(a+" ");
            }
            System.out.println();
        } else if (Thread.currentThread().isInterrupted()) {
            this.taskState = TaskState.Aborted;
        }
    }


    public Set<Long> getPrimeFactors() throws IllegalStateException {
        return this.primeFactors;
    }


    public TaskState getState() {
        return this.taskState;
    }


}


